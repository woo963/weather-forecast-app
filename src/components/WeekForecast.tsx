import React, {useEffect, useState} from "react";
import {IDayWeather} from "../constants";
import {fetchWeekWeather} from "../RequestWeather";
import {WeatherPlaceholder} from "./WeatherPlaceholder";
import {getSelectedCity} from "../utils";
import {Text, View} from "react-native";
import WeekWeatherCards from "./WeekWeatherCards";
import {styles} from "./forecastBlock.module";
import SelectCity from "./SelectCity";

export default function WeekForecast() {
  const [city, setCity] = useState<string>('Select')
  const [forecast, setForecast] = useState<IDayWeather[]>([])
  const [loading, setLoading] = useState<boolean>(false)

  useEffect(() => {
    if (city !== 'Select') {
      const selectedCity = getSelectedCity(city)
      fetchWeekWeather(selectedCity, setForecast, setLoading).then()
    } else setLoading(false)
  }, [city])


  return (
    <View style={[styles.forecastBlock, {height: 464}]}>
      <Text style={styles.forecastBlock__title}>7 Days Forecast</Text>
      <SelectCity setCity={setCity}/>
      {
        (loading)
          ? <WeekWeatherCards forecast={forecast}/>
          : <WeatherPlaceholder/>
      }
    </View>
  )
}
