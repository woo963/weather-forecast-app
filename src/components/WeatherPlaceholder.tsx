import React from "react";
import {Image, StyleSheet, Text, View} from "react-native";

export const WeatherPlaceholder = () => {
  return(
    <View style={styles.plugBlock}>
      <Image source={require('../../assets/Placeholder/Academy-Weather-bg160.png')} style={styles.plugBlock__img}/>
      <Text style={styles.plugBlock__text}>Fill in all the fields and the weather will be displayed</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  plugBlock: {
    width: 240,
    marginVertical: 9,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FFF'
  },
  plugBlock__img: {
    width: 150,
    height: 150,
  },
  plugBlock__text: {
    fontWeight: 'bold',
    marginTop: 24,
    fontSize: 16,
    textAlign: 'center',
    color: '#8083A4',
  }
})
