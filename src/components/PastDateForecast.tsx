import React, {useEffect, useState} from "react";
import {cityArr, IDayWeather, months, patternInputDate} from "../constants";
import {fetchPastWeather} from "../RequestWeather";
import {WeatherPlaceholder} from "./WeatherPlaceholder";
import {getSelectedCity} from "../utils";
import {Pressable, Text, View} from "react-native";
import DateTimePicker from '@react-native-community/datetimepicker';
import WeatherCard from "./WeatherCard";
import {styles} from "./forecastBlock.module";
import SelectCity from "./SelectCity";

export default function PastDateForecast() {
  const [city, setCity] = useState<string>('')
  const [date, setDate] = useState<Date>(new Date())
  const [show, setShow] = useState<boolean>(false)
  const [forecast, setForecast] = useState<IDayWeather>({
    temp: 0,
    icon: '',
    date: ''
  })
  const [loading, setLoading] = useState<boolean>(false)

  useEffect(() => {
    if (city !== 'Select') {
      const selectedCity = getSelectedCity(city)
      const dateForCard = date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear()
      fetchPastWeather(selectedCity, (Math.floor(date.getTime()/1000)).toString(), setForecast, setLoading, dateForCard).then()
    } else setLoading(false)
  }, [city, date])

  const minimumDate = () => {
    const nowDate = new Date()
    return nowDate.getTime() - 432000000
  }

  const showDatepicker = () => {
    setShow(true)
  }

  return (
    <View style={{...styles.forecastBlock,...{height: 576}}}>
      <Text style={styles.forecastBlock__title}>Forecast for a Date in the Past</Text>
      <SelectCity setCity={setCity}/>
      <Pressable onPress={showDatepicker} style={styles.forecastBlock__select}>
        <Text style={styles.forecastBlock__select_item}>{`${date.getDate()}/${months[date.getMonth()]}/${date.getFullYear()}`}</Text>
      </Pressable>
      {show && <DateTimePicker
        value={date}
        mode="date"
        onChange={(e, selectedDate) => {const currentDate = selectedDate || date; setDate(currentDate); setShow(false)}}
        maximumDate={new Date()}
        minimumDate={new Date(minimumDate())}
        style={styles.forecastBlock__select}
      />}
      {
        (loading)
          ? <WeatherCard forecast={forecast} className="past"/>
          : <WeatherPlaceholder/>
      }
    </View>
  )
}

