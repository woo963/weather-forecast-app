import {IDayWeather} from "../constants";
import {Image, StyleSheet, Text, View} from "react-native";
import React from "react";

export default function WeatherCard(props: { className: string, forecast: IDayWeather }) {
  let dayWeatherCard;
  let iconStyle;
  if (props.className === 'now') {
    dayWeatherCard = {...styles.dayWeatherCard, ...styles.now}
    iconStyle = styles.dayWeatherCard__icon
  } else {
    dayWeatherCard = {...styles.dayWeatherCard, ...styles.past}
    iconStyle = {...styles.dayWeatherCard__icon, ...styles.icon_past}
  }
  return (
    <View style={dayWeatherCard}>
      <Text style={styles.dayWeatherCard__date}>{props.forecast.date}</Text>
      <Image source={{uri: `https://openweathermap.org/img/wn/${props.forecast.icon}@2x.png`}}
             style={iconStyle}/>
      <Text style={styles.dayWeatherCard__temp}>+{props.forecast.temp}°</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  dayWeatherCard: {
    backgroundColor: '#373AF5',
    borderWidth: 2,
    borderColor: '#2C2D76',
    borderRadius: 8,
    shadowColor: 'rgba(4, 5, 73, 0.25)',
    borderStyle: 'solid',
    position: "relative",
    shadowOffset: {width: 14, height: 20},
    shadowRadius: 4,
    elevation: 7
  },
  now: {
    width: 174,
    height: 238,
    marginRight: 24
  },
  past: {
    width: 252,
    height: 238,
  },
  dayWeatherCard__date: {
    position: "absolute",
    color: '#FFFFFF',
    top: 20,
    left: 20,
    fontFamily: 'Ubuntu-bold'
  },
  dayWeatherCard__icon: {
    position: "absolute",
    top: 35,
    left: 5,
    width: 160,
    height: 160
  },
  icon_past: {
    left: 45,
  },
  dayWeatherCard__temp: {
    position: "absolute",
    color: '#FFFFFF',
    bottom: 20,
    right: 20,
    fontFamily: 'Ubuntu-bold',
    fontSize: 32,
  }
})