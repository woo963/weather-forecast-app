import {ScrollView, StyleSheet, View} from "react-native";
import React from "react";
import WeatherCard from "./WeatherCard";
import {IDayWeather} from "../constants";

export default function WeekWeatherCards (props: { forecast: IDayWeather[]}) {
  return (
    <ScrollView horizontal={true} style={styles.dailyForecastBlock}>
      <View style={styles.dailyForecastBlock__cards}>
        {props.forecast.map((item: IDayWeather, index: number) => <WeatherCard forecast={item} className={'now'} key={index}/>)}
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  dailyForecastBlock:{
    height: 300,
    marginTop: 6
  },
  dailyForecastBlock__cards:{
    flexDirection: "row",
    height: 300,
  },
})