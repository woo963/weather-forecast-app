import {View} from "react-native";
import {styles} from "./forecastBlock.module";
import {Picker} from "@react-native-picker/picker";
import React, {useEffect, useState} from "react";
import {cityArr} from "../constants";

type CityFontColor = {
  marginLeft: 10,
  color: string
}

export default function SelectCity(props: { setCity: (arg: string) => void }) {
  const [city, setCity] = useState<string>('Select')
  const [cityFontColor, setCityFontColor] = useState<CityFontColor>({marginLeft: 10, color: '#8083A4'})

  const selectList = () => {
    const d = cityArr.map((item, index) => <Picker.Item label={item.name} value={item.name} key={index}/>)
    d.unshift(<Picker.Item label="Select" value={'Select'} key={-1}/>)
    return d
  }

  function onSelectCity(newCity: string): void {
    setCity(newCity)
  }

  useEffect(() => {
    if (city !== 'Select') {
      setCityFontColor({marginLeft: 10, color: '#2C2D76'})
    } else setCityFontColor({marginLeft: 10, color: '#8083A4'})
    props.setCity(city)
  }, [city])

  return (
    <View style={styles.forecastBlock__select}>
      <Picker
        selectedValue={city}
        onValueChange={(item) => onSelectCity(item)}
        style={[styles.forecastBlock__select_item, cityFontColor]}
        itemStyle={styles.forecastBlock__select_item}
      >
        {selectList()}
      </Picker>
    </View>
  )
}