import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
  forecastBlock: {
    backgroundColor: "#FFF",
    borderRadius: 8,
    marginVertical: 10,
    padding: 24,
    shadowColor: 'rgba(4, 5, 73, 0.25)',
    shadowOffset: { width: 14, height: 20 },
    shadowRadius: 4,
    elevation: 3
  },
  forecastBlock__title: {
    color: "#2C2D76",
    fontSize: 32,
    marginBottom: 32,
    marginTop: 8,
    fontFamily: 'Ubuntu-bold'
  },
  forecastBlock__select: {
    width: 252,
    height: 48,
    borderRadius: 8,
    borderStyle: "solid",
    borderWidth: 2,
    borderColor: 'rgba(128, 131, 164, 0.2)',
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
    marginBottom: 24,
    fontFamily: 'Ubuntu-normal'
  },
  forecastBlock__select_item: {
    fontSize: 16,
    marginVertical: 12,
    marginLeft: 16,
    color:'#2C2D76',
    fontFamily: 'Ubuntu-normal'
  }
})