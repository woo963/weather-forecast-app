import React from 'react';
import {ImageBackground, ScrollView, StyleSheet, Text, View} from 'react-native';
import WeekForecast from "./src/components/WeekForecast";
import PastDateForecast from "./src/components/PastDateForecast";
import {useFonts} from 'expo-font';
import AppLoading from "expo-app-loading";

export default function App() {
  let [fontsLoaded] = useFonts({
    'Ubuntu-bold': require('./assets/fonts/Ubuntu-Bold.ttf'),
    'Ubuntu-normal': require('./assets/fonts/Ubuntu-Regular.ttf')
  })
  if (!fontsLoaded) {
    return <AppLoading/>;
  } else {
    return (
      <ImageBackground
        style={[styles.background, {backgroundColor: '#373AF5'}]}
        source={require('./assets/Background/Bg-up.png')}
      >
        <ImageBackground
          style={styles.background}
          source={require('./assets/Background/Bg-buttom.png')}
        >
          <View style={styles.container}>
            <View style={styles.header}>
              <Text style={styles.title}>
                Weather <View style={styles.title2}><Text style={styles.title}>forecast</Text></View>
              </Text>
            </View>
            <ScrollView style={styles.main} showsVerticalScrollIndicator={false}>
              <WeekForecast/>
              <PastDateForecast/>
            </ScrollView>
            <Text style={styles.footer}>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Text>
          </View>
        </ImageBackground>
      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'contain',
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    width: 240,
    marginTop: 40,
  },
  title: {
    color: '#FFFFFF',
    fontSize: 32,
    fontFamily: 'Ubuntu-bold'
  },
  title2: {
    transform: [{translateY: -10}, {translateX: 117}]
  },
  main: {
    width: 300
  },
  footer: {
    fontSize: 14,
    lineHeight: 18,
    color: '#FFFFFF',
    opacity: 0.6,
    textAlign: 'center',
    marginTop: 18,
    marginBottom: 16,
    fontFamily: 'Ubuntu-normal'
  },
});
